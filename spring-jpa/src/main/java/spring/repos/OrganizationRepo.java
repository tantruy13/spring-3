package spring.repos;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import spring.entities.Organization;

/**
 * Created by Tan on 4/7/2017.
 */
@Repository
public interface OrganizationRepo extends CrudRepository<Organization,Integer> {


}
