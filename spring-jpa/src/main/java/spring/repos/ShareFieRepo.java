package spring.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import spring.entities.SharedFile;

/**
 * Created by Tan on 4/7/2017.
 */
@Repository
public interface ShareFieRepo extends CrudRepository<SharedFile,Long> {
}
