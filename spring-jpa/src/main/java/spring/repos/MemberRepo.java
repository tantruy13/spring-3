package spring.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import spring.entities.Member;


/**
 * Created by Tan on 4/7/2017.
 */
@Repository
public interface MemberRepo extends CrudRepository<Member, Long> {
}
