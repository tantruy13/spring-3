package spring.repos;

import spring.entities.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * * Created by Tan on 4/6/2017.
 */
@Repository
public interface PostRepo extends CrudRepository<Post, Long> {

}
