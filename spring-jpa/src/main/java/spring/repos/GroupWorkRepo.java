package spring.repos;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import spring.entities.GroupWork;

import java.util.List;

/**
 * Created by Tan on 4/7/2017.
 */
@Repository
public interface GroupWorkRepo extends CrudRepository<GroupWork,Integer> {

    @Query("SELECT a FROM GroupWork  AS a WHERE a.organization.organizationID = :id")
    List<GroupWork> getGroupWordByOrgID(@Param("id") Integer id);

}
