package spring.repos;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import spring.entities.Account;

/**
 * Created by Tan on 4/8/2017.
 */
@Repository
public interface AccountRepo extends CrudRepository<Account,String> {

}
