package spring.entities;

import javax.persistence.*;

/**
 * Created by Tan on 4/8/2017.
 */
@Entity
@Table(name = "Account")
public class Account {
    @Id
    private String accountName;
    private String memberPassWord;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getMemberPassWord() {
        return memberPassWord;
    }

    public void setMemberPassWord(String memberPassWord) {
        this.memberPassWord = memberPassWord;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="memberID",nullable=false)
    private Member member;
}
