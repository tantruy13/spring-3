package spring.entities;

import javax.persistence.*;

/**
 * Created by Tan on 4/7/2017.
 */
@Entity
@Table(name = "SharedFile")
public class SharedFile {
    @Id
    @GeneratedValue
    private long fileID ;
    private String fileName;
    private String fileContent;

    public long getFileID() {
        return fileID;
    }

    public void setFileID(long fileID) {
        this.fileID = fileID;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="memberID",nullable=true)
    private Member member;
}
