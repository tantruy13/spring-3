package spring.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Tan on 4/7/2017.
 */
@Entity
@Table(name = "Member")
public class Member {

    @Id
    @GeneratedValue
    @Column(name = "memberID")
    private long memberID;
    private String accountName;
    private String firstName;
    private String lastName;
    private String gender;
    private Date birthDate;
    private String address;
    private String phoneNumber;
    private String email;
    private  String ProfilePicture;

    public long getMemberID() {
        return memberID;
    }

    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }


    public void setMemberID(long memberID) {
        this.memberID = memberID;
    }

    public GroupWork getGroupWork() {
        return groupWork;
    }

    public void setGroupWork(GroupWork groupWork) {
        this.groupWork = groupWork;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<SharedFile> getSharedFileList() {
        return sharedFileList;
    }

    public void setSharedFileList(List<SharedFile> sharedFileList) {
        this.sharedFileList = sharedFileList;
    }

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="groupWorkID",nullable=true)
    private GroupWork groupWork;


    @OneToOne
    private Account account;

    @OneToMany(cascade = CascadeType.ALL
            ,mappedBy="member")
    private List<SharedFile> sharedFileList;
}
