package spring.entities;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Tan on 4/7/2017.
 */
@Entity
@Table(name = "Organization")
public class Organization {
    @Id
    @GeneratedValue
    @Column(name = "organizationID")
    public int organizationID;
    private String organizationName;

    public int getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(int organizationID) {
        this.organizationID = organizationID;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public List<GroupWork> getGroupWorks() {
        return groupWorks;
    }

    public void setGroupWorks(List<GroupWork> groupWorks) {
        this.groupWorks = groupWorks;
    }

    @OneToMany(mappedBy="organization")
    private List<GroupWork> groupWorks;

}

