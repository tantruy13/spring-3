package spring.entities;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Tan on 4/7/2017.
 */
@Entity
@Table(name = "GroupWork")
public class GroupWork {
    @Id
    @GeneratedValue
    @Column(name = "groupWorkID")
    private int groupWorkID;

    private String groupWorkName;

    public int getGroupWorkID() {
        return groupWorkID;
    }

    public void setGroupWorkID(int groupWorkID) {
        this.groupWorkID = groupWorkID;
    }

    public String getGroupWorkName() {
        return groupWorkName;
    }

    public void setGroupWorkName(String groupWorkName) {
        this.groupWorkName = groupWorkName;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @OneToMany(cascade = CascadeType.ALL
            ,mappedBy="groupWork")
    public List<Member> members;



    @ManyToOne
   @JoinColumn(name="organizationID",nullable=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    public Organization organization;

    @PostPersist
    public void isNull(){
        organization=null;
    }



}
