package spring.client;

/**
 * Created by Tan on 4/8/2017.
 */
public class OrganizationModel {

    private int organizationID;
    private String organizationName;

    public int getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(int organizationID) {
        this.organizationID = organizationID;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }
}
