package spring.client;

/**
 * Created by Tan on 4/8/2017.
 */
public class GroupWorkModel {

    private int groupWorkID;

    private String groupWorkName;

    public int getGroupWorkID() {
        return groupWorkID;
    }

    public void setGroupWorkID(int groupWorkID) {
        this.groupWorkID = groupWorkID;
    }

    public String getGroupWorkName() {
        return groupWorkName;
    }

    public void setGroupWorkName(String groupWorkName) {
        this.groupWorkName = groupWorkName;
    }
}
