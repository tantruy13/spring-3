package spring.client;

/**
 * Created by Tan on 4/8/2017.
 */
public class AccountModel {

    private String accountName;
    private String memberPassWord;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getMemberPassWord() {
        return memberPassWord;
    }

    public void setMemberPassWord(String memberPassWord) {
        this.memberPassWord = memberPassWord;
    }
}
