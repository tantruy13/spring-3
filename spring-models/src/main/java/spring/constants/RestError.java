package spring.constants;

/**
 *  * Created by Tan on 4/6/2017.
 */
public interface RestError {
    int ERROR_DB = -100;
    int NOT_FOUND = -101;
    int SUCCESS = 0;
}
