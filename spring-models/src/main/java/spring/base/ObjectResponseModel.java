package spring.base;

import java.util.List;

/**
 *  * Created by Tan on 4/6/2017.
 */
public class ObjectResponseModel<T> {
    private int errorCode;
    private String desc;
    private T content;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

}
