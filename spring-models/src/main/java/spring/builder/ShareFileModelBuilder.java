package spring.builder;

import spring.client.ShareFileModel;

/**
 * Created by Tan on 4/8/2017.
 */
public final class ShareFileModelBuilder {
    private long fileID ;
    private String fileName;
    private String fileContent;

    private ShareFileModelBuilder() {
    }

    public static ShareFileModelBuilder anUploadFileModel() {
        return new ShareFileModelBuilder();
    }

    public ShareFileModelBuilder withFileID(long fileID) {
        this.fileID = fileID;
        return this;
    }

    public ShareFileModelBuilder withFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public ShareFileModelBuilder withFileContent(String fileContent) {
        this.fileContent = fileContent;
        return this;
    }

    public ShareFileModel build() {
        ShareFileModel uploadFileModel = new ShareFileModel();
        uploadFileModel.setFileID(fileID);
        uploadFileModel.setFileName(fileName);
        uploadFileModel.setFileContent(fileContent);
        return uploadFileModel;
    }
}
