package spring.builder;

import spring.client.GroupWorkModel;

/**
 * Created by Tan on 4/8/2017.
 */
public final class GorupWorkModelBuilder {

    private int groupWorkID;
    private String groupWorkName;

    private GorupWorkModelBuilder() {
    }

    public static GorupWorkModelBuilder aGorupWorkModel() {
        return new GorupWorkModelBuilder();
    }

    public GorupWorkModelBuilder withGroupWorkID(int groupWorkID) {
        this.groupWorkID = groupWorkID;
        return this;
    }

    public GorupWorkModelBuilder withGroupWorkName(String groupWorkName) {
        this.groupWorkName = groupWorkName;
        return this;
    }

    public GroupWorkModel build() {
        GroupWorkModel gorupWorkModel = new GroupWorkModel();
        gorupWorkModel.setGroupWorkID(groupWorkID);
        gorupWorkModel.setGroupWorkName(groupWorkName);
        return gorupWorkModel;
    }
}
