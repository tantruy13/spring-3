package spring.builder;

import spring.client.PostModel;

import java.util.Date;

/**
 * Created by Tan on 4/8/2017.
 */
public final class PostModelBuilder {
    public long id;
    public String title;
    public String content;
    public Date createdTimestamp;
    public Date publishedDate;
    public String state;

    private PostModelBuilder() {
    }

    public static PostModelBuilder aPostModel() {
        return new PostModelBuilder();
    }

    public PostModelBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public PostModelBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public PostModelBuilder withContent(String content) {
        this.content = content;
        return this;
    }

    public PostModelBuilder withCreatedTimestamp(Date createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
        return this;
    }

    public PostModelBuilder withPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
        return this;
    }

    public PostModelBuilder withState(String state) {
        this.state = state;
        return this;
    }

    public PostModel build() {
        PostModel postModel = new PostModel();
        postModel.setId(id);
        postModel.setTitle(title);
        postModel.setContent(content);
        postModel.setCreatedTimestamp(createdTimestamp);
        postModel.setPublishedDate(publishedDate);
        postModel.setState(state);
        return postModel;
    }
}
