package spring.builder;

import spring.client.OrganizationModel;

/**
 * Created by Tan on 4/8/2017.
 */
public final class OrganizationModelBuilder {
    private int organizationID;
    private String organizationName;

    private OrganizationModelBuilder() {
    }

    public static OrganizationModelBuilder anOrganizationModel() {
        return new OrganizationModelBuilder();
    }

    public OrganizationModelBuilder withOrganizationID(int organizationID) {
        this.organizationID = organizationID;
        return this;
    }

    public OrganizationModelBuilder withOrganizationName(String organizationName) {
        this.organizationName = organizationName;
        return this;
    }

    public OrganizationModel build() {
        OrganizationModel organizationModel = new OrganizationModel();
        organizationModel.setOrganizationID(organizationID);
        organizationModel.setOrganizationName(organizationName);
        return organizationModel;
    }
}
