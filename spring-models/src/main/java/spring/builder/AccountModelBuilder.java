package spring.builder;

import spring.client.AccountModel;

/**
 * Created by Tan on 4/8/2017.
 */
public final class AccountModelBuilder {

    private String accountName;
    private String memberPassWord;

    private AccountModelBuilder() {
    }

    public static AccountModelBuilder anAccountModel() {
        return new AccountModelBuilder();
    }

    public AccountModelBuilder withAccountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public AccountModelBuilder withMemberPassWord(String memberPassWord) {
        this.memberPassWord = memberPassWord;
        return this;
    }

    public AccountModel build() {
        AccountModel accountModel = new AccountModel();
        accountModel.setAccountName(accountName);
        accountModel.setMemberPassWord(memberPassWord);
        return accountModel;
    }
}
