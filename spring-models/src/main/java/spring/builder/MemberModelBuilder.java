package spring.builder;

import spring.client.MemberModel;

import java.util.Date;

/**
 * Created by Tan on 4/8/2017.
 */
public final class MemberModelBuilder {
    private long memberID;
    private String accountName;
    private String firstName;
    private String lastName;
    private String gender;
    private Date birthDate;
    private String address;
    private String phoneNumber;
    private String email;
    private  String ProfilePicture;

    private MemberModelBuilder() {
    }

    public static MemberModelBuilder aMemberModel() {
        return new MemberModelBuilder();
    }

    public MemberModelBuilder withMemberID(long memberID) {
        this.memberID = memberID;
        return this;
    }

    public MemberModelBuilder withAccountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public MemberModelBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public MemberModelBuilder withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public MemberModelBuilder withGender(String gender) {
        this.gender = gender;
        return this;
    }

    public MemberModelBuilder withBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public MemberModelBuilder withAddress(String address) {
        this.address = address;
        return this;
    }

    public MemberModelBuilder withPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public MemberModelBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public MemberModelBuilder withProfilePicture(String ProfilePicture) {
        this.ProfilePicture = ProfilePicture;
        return this;
    }

    public MemberModel build() {
        MemberModel memberModel = new MemberModel();
        memberModel.setMemberID(memberID);
        memberModel.setAccountName(accountName);
        memberModel.setFirstName(firstName);
        memberModel.setLastName(lastName);
        memberModel.setGender(gender);
        memberModel.setBirthDate(birthDate);
        memberModel.setAddress(address);
        memberModel.setPhoneNumber(phoneNumber);
        memberModel.setEmail(email);
        memberModel.setProfilePicture(ProfilePicture);
        return memberModel;
    }
}
