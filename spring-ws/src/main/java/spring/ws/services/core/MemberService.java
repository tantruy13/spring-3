package spring.ws.services.core;

import spring.base.ObjectResponseModel;
import spring.client.MemberModel;

/**
 * Created by Tan on 4/8/2017.
 */
public interface MemberService {
    // abstrach function return respone object
    ObjectResponseModel<MemberModel> getMemberById(long id);
}
