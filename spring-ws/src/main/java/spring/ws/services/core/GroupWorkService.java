package spring.ws.services.core;

import spring.base.ObjectResponseModel;
import spring.client.GroupWorkModel;

import java.util.List;

/**
 * Created by Tan on 4/8/2017.
 */
public interface GroupWorkService {
    ObjectResponseModel<GroupWorkModel> getGroupById(long id);

    ObjectResponseModel<List<GroupWorkModel>> getGroups(long id);
    // abstrach function return respone object
}
