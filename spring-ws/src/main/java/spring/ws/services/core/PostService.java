package spring.ws.services.core;

import spring.base.ObjectResponseModel;
import spring.client.PostModel;

/**
 * Created by Tan on 4/6/2017.
 */
public interface PostService {
    // abstrach function return respone object
    ObjectResponseModel<PostModel> getPost(long id);
}
