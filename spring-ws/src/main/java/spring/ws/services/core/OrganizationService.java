package spring.ws.services.core;

import spring.base.ObjectResponseModel;
import spring.client.OrganizationModel;

/**
 * Created by Tan on 4/8/2017.
 */
public interface OrganizationService {
    ObjectResponseModel<OrganizationModel> getOrganizeByID(long id);

    ObjectResponseModel<OrganizationModel> deleteOrganizationById(long id);
}
