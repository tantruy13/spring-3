package spring.ws.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.base.ObjectResponseModel;
import spring.constants.RestError;
import spring.entities.Organization;
import spring.client.OrganizationModel;
import spring.repos.OrganizationRepo;
import spring.ws.services.core.OrganizationService;

import java.util.Objects;

import static spring.builder.OrganizationModelBuilder.anOrganizationModel;

/**
 * Created by Tan on 4/8/2017.
 */
@Service
public class OrganizationServiceImpl implements OrganizationService{
    // logger
    private static final Logger logger = LoggerFactory.getLogger(OrganizationServiceImpl.class);
    // load repo
    @Autowired
    OrganizationRepo organizationRepo;
    // function


    // build to model object
    private OrganizationModel toOrganizationModel(Organization organization){
        return  anOrganizationModel().withOrganizationID(organization.getOrganizationID()).withOrganizationName(organization.getOrganizationName()).build();
    }

    @Override
    public ObjectResponseModel<OrganizationModel> getOrganizeByID(long id) {
        ObjectResponseModel<OrganizationModel> memberModel = new ObjectResponseModel<>();
        try {
            Organization one = organizationRepo.findOne((int)id);
            Objects.requireNonNull(one);
            memberModel.setErrorCode(RestError.SUCCESS);
            memberModel.setContent(toOrganizationModel(one));
        } catch (NullPointerException e) {
            memberModel.setErrorCode(RestError.ERROR_DB);
        } catch (Exception e) {
            memberModel.setDesc(e.getMessage());
            memberModel.setErrorCode(RestError.ERROR_DB);
        }
        return memberModel;
    }

    @Override
    public ObjectResponseModel<OrganizationModel> deleteOrganizationById(long id) {
        ObjectResponseModel<OrganizationModel> memberModel = new ObjectResponseModel<>();
        try {
             organizationRepo.delete((int)id);
            memberModel.setErrorCode(RestError.SUCCESS);
        } catch (NullPointerException e) {
            memberModel.setErrorCode(RestError.ERROR_DB);
        } catch (Exception e) {
            memberModel.setDesc(e.getMessage());
            memberModel.setErrorCode(RestError.ERROR_DB);
        }
        return memberModel;
    }
}
