package spring.ws.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.base.ObjectResponseModel;
import spring.constants.RestError;
import spring.entities.Member;
import spring.client.MemberModel;
import spring.repos.MemberRepo;
import spring.ws.services.core.MemberService;

import java.util.Objects;

import static spring.builder.MemberModelBuilder.aMemberModel;


/**
 * Created by Tan on 4/8/2017.
 */
@Service
public class MemberServiceImpl implements MemberService {
    // logger
    private static final Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);
    // load repo
    @Autowired
    private MemberRepo memberRepo;
    // function
    @Override
    public ObjectResponseModel<MemberModel> getMemberById(long id) {
        ObjectResponseModel<MemberModel> memberModel = new ObjectResponseModel<>();
        try {
            Member one = memberRepo.findOne(id);
            Objects.requireNonNull(one);
            memberModel.setErrorCode(RestError.SUCCESS);
            memberModel.setContent(toMemberModel(one));
        } catch (NullPointerException e) {
            memberModel.setErrorCode(RestError.ERROR_DB);
        } catch (Exception e) {
            memberModel.setDesc(e.getMessage());
            memberModel.setErrorCode(RestError.ERROR_DB);
        }
        return memberModel;
    }
    // build to model object
    private MemberModel toMemberModel(Member member) {
        logger.info("date : {}",member.getBirthDate());
        return aMemberModel().withProfilePicture(member.getProfilePicture()).withPhoneNumber(member.getPhoneNumber()).withFirstName(member.getFirstName()).withLastName(member.getLastName())
                .withEmail(member.getEmail()).withAccountName(member.getAccountName()).withAddress(member.getAddress()).withBirthDate(member.getBirthDate())
                .withGender(member.getGender()).withMemberID(member.getMemberID()).withGender(member.getBirthDate().toString()).build();
    }
}
