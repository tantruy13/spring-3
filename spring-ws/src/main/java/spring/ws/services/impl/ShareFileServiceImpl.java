package spring.ws.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.entities.SharedFile;
import spring.client.ShareFileModel;
import spring.repos.ShareFieRepo;
import spring.ws.services.core.ShareFileService;

import static spring.builder.ShareFileModelBuilder.anUploadFileModel;

/**
 * Created by Tan on 4/8/2017.
 */
@Service
public class ShareFileServiceImpl implements ShareFileService {
    //logger
    private static final Logger logger = LoggerFactory.getLogger(ShareFileServiceImpl.class);
    // load repo
    @Autowired
    ShareFieRepo shareFieRepo;
    // functions

    // to model object
    private ShareFileModel toShareFileModel(SharedFile sf){
        return anUploadFileModel().withFileID(sf.getFileID()).withFileName(sf.getFileName()).withFileContent(sf.getFileContent()).build();
    }
}
