package spring.ws.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.base.ObjectResponseModel;
import spring.constants.RestError;
import spring.entities.GroupWork;
import spring.client.GroupWorkModel;
import spring.repos.GroupWorkRepo;
import spring.ws.services.core.GroupWorkService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static spring.builder.GorupWorkModelBuilder.aGorupWorkModel;

/**
 * Created by Tan on 4/8/2017.
 */
@Service
public class GroupWorkServiceImpl implements GroupWorkService {
    // logger
    private static final Logger logger = LoggerFactory.getLogger(GroupWorkServiceImpl.class);
    // load repo
    @Autowired
    GroupWorkRepo groupWorkRepo;
    // functions CRUD


    // build to model object
    private GroupWorkModel toGroupWorkModel(GroupWork gw){
        return aGorupWorkModel().withGroupWorkID(gw.getGroupWorkID()).withGroupWorkName(gw.getGroupWorkName()).build();

    }
    // build to list model

    public List<GroupWorkModel> toListModel(List<GroupWork> gw){
        List<GroupWorkModel> groupWorkModels = new ArrayList<>();
        for (GroupWork g: gw) {
            groupWorkModels.add(toGroupWorkModel(g));
        }
        return groupWorkModels;
    }

    @Override
    public ObjectResponseModel<GroupWorkModel> getGroupById(long id) {
        ObjectResponseModel<GroupWorkModel> memberModel = new ObjectResponseModel<>();
        try {
            GroupWork one = groupWorkRepo.findOne((int)id);
            Objects.requireNonNull(one);
            memberModel.setErrorCode(RestError.SUCCESS);
            memberModel.setContent(toGroupWorkModel(one));
        } catch (NullPointerException e) {
            memberModel.setErrorCode(RestError.ERROR_DB);
        } catch (Exception e) {
            memberModel.setDesc(e.getMessage());
            memberModel.setErrorCode(RestError.ERROR_DB);
        }
        return memberModel;
    }

    @Override
    public ObjectResponseModel<List<GroupWorkModel>> getGroups(long id) {
        ObjectResponseModel<List<GroupWorkModel>> memberModel = new ObjectResponseModel<>();
        try {
            logger.info("id : {} ",id);
            List<GroupWork> groupWorks = groupWorkRepo.getGroupWordByOrgID((int)id);
            logger.info("data : ---------- {}",groupWorks);
            Objects.requireNonNull(groupWorks);
            memberModel.setErrorCode(RestError.SUCCESS);
            memberModel.setContent(toListModel(groupWorks));
        } catch (NullPointerException e) {
            memberModel.setErrorCode(RestError.ERROR_DB);
        } catch (Exception e) {
            memberModel.setDesc(e.getMessage());
            memberModel.setErrorCode(RestError.ERROR_DB);
        }
        return memberModel;
    }



}
