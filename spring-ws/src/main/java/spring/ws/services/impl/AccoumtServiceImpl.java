package spring.ws.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.entities.Account;
import spring.client.AccountModel;
import spring.repos.AccountRepo;
import spring.ws.services.core.AccountService;

import static spring.builder.AccountModelBuilder.anAccountModel;

/**
 * Created by Tan on 4/8/2017.
 */
@Service
public class AccoumtServiceImpl implements AccountService {
    // logger
    private static final Logger logger = LoggerFactory.getLogger(AccoumtServiceImpl.class);
    // load respo
    @Autowired
    AccountRepo accountRepo;
    // function CRUD



    // build to model object
    private AccountModel toAccountModel(Account ac){
        return anAccountModel().withAccountName(ac.getAccountName()).withMemberPassWord(ac.getMemberPassWord()).build();
    }
}
