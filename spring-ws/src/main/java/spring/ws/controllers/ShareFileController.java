package spring.ws.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import spring.ws.services.core.ShareFileService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Tan on 4/8/2017.
 */
public class ShareFileController {
    // logger
    static final Logger logger = LoggerFactory.getLogger(ShareFileController.class);
    // load service
    @Autowired
    ShareFileService saveFileService;
    // controller functions
    @GET
    @Path("/ok")
    @Produces(MediaType.TEXT_PLAIN)
    public Response check() {
        return Response.ok("OK").build();
    }
}
