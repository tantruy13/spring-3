package spring.ws.controllers;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import spring.entities.Organization;
import spring.ws.services.core.GroupWorkService;
import spring.ws.services.core.OrganizationService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Tan on 4/8/2017.
 */
public class OrganizationController {
    //logger
    static final Logger logger = LoggerFactory.getLogger(OrganizationController.class);
    // load service
    @Autowired
    OrganizationService organizationService;
    // controller functions
    @GET
    @Path("/ok")
    @Produces(MediaType.TEXT_PLAIN)
    public Response check() {
        return Response.ok("OK").build();
    }


    @GET
    @Path("/getOrganizeByID/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOranizationByID(@PathParam("id") long id){
        return Response.ok(organizationService.getOrganizeByID(id)).build();
    }

    @GET
    @Path("/deleteOrganizationById/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteOrganizationById(@PathParam("id") long id){
        return Response.ok(organizationService.deleteOrganizationById(id)).build();
    }
    @POST
    @Path("/addOrganization")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addOrganization(Organization or){
        logger.info("this is organization :  {}", ToStringBuilder.reflectionToString(or));
        return Response.ok("OK").build();
    }

}
