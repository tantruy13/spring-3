package spring.ws.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import spring.ws.services.core.AccountService;
import spring.ws.services.core.GroupWorkService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Tan on 4/8/2017.
 */
public class GroupWorkController {
    // logger
    static final Logger logger = LoggerFactory.getLogger(GroupWorkController.class);
    //load service
    @Autowired
    GroupWorkService groupWorkService;
    // controller function
    @GET
    @Path("/ok")
    @Produces(MediaType.TEXT_PLAIN)
    public Response check() {
        return Response.ok("OK").build();
    }


    @GET
    @Path("/getOrganizeByID/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGroupById(@PathParam("id") long id){
        return Response.ok(groupWorkService.getGroupById(id)).build();
    }


    @GET
    @Path("/getGroups/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGroups(@PathParam("id") long id){
        return Response.ok(groupWorkService.getGroups(id)).build();
    }

}
