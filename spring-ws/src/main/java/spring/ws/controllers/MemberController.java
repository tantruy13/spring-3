package spring.ws.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import spring.ws.services.core.MemberService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Tan on 4/8/2017.
 */
@Path("/")
public class MemberController {
    // logger
    static final Logger logger = LoggerFactory.getLogger(MemberController.class);
    // load service
    @Autowired
    MemberService memberService;
    // controllelr functions
    @GET
    @Path("/ok")
    @Produces(MediaType.TEXT_PLAIN)
    public Response check() {
        return Response.ok("OK").build();
    }


    @GET
    @Path("/getMember/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPost(@PathParam("id") long id) {
        logger.info("Getting PostModel for {}", id);
        return Response.ok(memberService.getMemberById(id)).build();
    }
}
