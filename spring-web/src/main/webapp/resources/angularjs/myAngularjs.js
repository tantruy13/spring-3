var app = angular.module('geek-app',[]);

app.controller('getPostController', function($scope,$http) {
    console.log('enter the scope;');
    $scope.name = "This is test page";
    $http.get("/post/byId/1")
    .then(function(response) {
        console.log(response);
        $scope.data = response.data.content;
    });
});