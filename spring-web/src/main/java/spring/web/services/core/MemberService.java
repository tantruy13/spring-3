package spring.web.services.core;

import spring.base.ObjectResponseModel;
import spring.client.MemberModel;

/**
 * Created by Tan on 4/8/2017.
 */
public interface MemberService {
    // return data object function
    ObjectResponseModel<MemberModel> getMember(long id);
}
