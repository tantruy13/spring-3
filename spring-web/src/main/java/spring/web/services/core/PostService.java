package spring.web.services.core;


import spring.base.ObjectResponseModel;
import spring.client.PostModel;

/**
 * Created by Tan on 4/6/2017.
 */
public interface PostService {
    // return data object function
    ObjectResponseModel<PostModel> getPost(long id);
}
