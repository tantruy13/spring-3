package spring.web.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import spring.web.services.core.GroupWorkService;

/**
 * Created by Tan on 4/8/2017.
 */
@Component
public class GroupWorkServiceImpl implements GroupWorkService {
    // logger
    private static Logger logger = LoggerFactory.getLogger(GroupWorkServiceImpl.class);
    // load template
    //url : spring.ws....
    @Autowired
    private RestTemplate restTemplate;

    @Value("${spring.ws.group}")
    private String getPostUrl;
    // override function
}
