package spring.web.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import spring.base.ObjectResponseModel;
import spring.client.MemberModel;
import spring.web.services.core.MemberService;

/**
 * Created by Tan on 4/8/2017.
 */
@Component
public class MemberServiceImpl implements MemberService {
    // logger
    private static Logger logger = LoggerFactory.getLogger(MemberServiceImpl.class);
    // load template
    //url : spring.ws....
    @Autowired
    private RestTemplate restTemplate;
    @Value("${spring.ws.member}")
    private String getPostUrl;
    // override function
    @Override
    public ObjectResponseModel<MemberModel> getMember(long id) {
        return null;
    }

}
