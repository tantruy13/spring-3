package spring.web.services.impl;

import spring.base.ObjectResponseModel;
import spring.client.PostModel;
import spring.web.services.core.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Tan on 4/6/2017.
 */
@Component
public class PostServiceImpl implements PostService {
    // logger
    private static Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);

    // load template
    // path is spring.ws
    @Autowired
    private RestTemplate restTemplate;
    @Value("${spring.ws.getpost}")
    private String getPostUrl;
    // override function
    @Override
    public ObjectResponseModel<PostModel> getPost(long id) {
        logger.info("Getting Post {} at {}", id, getPostUrl);
        ObjectResponseModel<PostModel> retValue = restTemplate.getForObject(getPostUrl + id, ObjectResponseModel.class);
        return retValue;
    }
}
