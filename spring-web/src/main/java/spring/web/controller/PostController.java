package spring.web.controller;

import spring.base.ObjectResponseModel;
import spring.client.PostModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import spring.web.services.core.PostService;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
/**
 * Created by Tan on 4/6/2017.
 */
@Controller

@RequestMapping("/post")
public class PostController {

    @Autowired
    PostService postService;



    @RequestMapping(value = "/byId/{id}", method = GET)
    @ResponseBody
    public ObjectResponseModel<PostModel> hello(@PathVariable("id") long id) {
        ObjectResponseModel<PostModel> post = postService.getPost(id);
        return post;
    }

}
