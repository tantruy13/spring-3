package spring.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Tan on 4/8/2017.
 */
@Controller
public class IndexController {


    @RequestMapping(name = "index", method = RequestMethod.GET)
    public String index(ModelMap mp){
  ;
        return "index";
    }
}
