package spring.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
/**
 * Created by Tan on 4/6/2017.
 */
@Controller
public class PagesController {

    @RequestMapping(value = "/post/{id}", method = GET)
    public String hello(@PathVariable("id") long id, ModelMap model) {
        Date d = new Date();
        model.addAttribute("date",d);
        return "post";
    }
}
