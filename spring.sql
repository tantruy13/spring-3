drop database if exists Spring;
create database Spring;
use Spring;
create table Organization(
OrganizationID mediumint primary key auto_increment,
OrganizationName varchar(50) not null
);
create table GroupWork(
GroupWorkID int primary key auto_increment,
GroupWorkName varchar(50) not null,
OrganizationID mediumint,
constraint fk_Organization_GroupWork foreign key (OrganizationID) references Organization(OrganizationID)on update cascade on delete set null
);
create table Member(
MemberID bigint primary key auto_increment, -- surrogate key
AccountName varchar(50) not null unique,
FirstName varchar(50) not null,
LastName varchar(50) not null,
Gender char(1) not null, -- 0:male / 1:female / 2:unknown
Birthdate date not null,
Address varchar(100) not null,
PhoneNumber varchar(22) not null,
Email varchar(100) not null,
ProfilePicture text,
GroupWorkID int,
constraint fk_GroupWork_Member foreign key (GroupWorkID) references GroupWork(GroupWorkID)on update cascade on delete set null
);
create table Account(
AccountName varchar(50) unique not null,
MemberPassword varchar(50) not null,
constraint fk_Member_Account foreign key (AccountName) references Member(AccountName)on update cascade on delete cascade
);
create table SharedFile(
FileID bigint primary key auto_increment,
FileName varchar(20) not null,
FileContent text not null,
CreatedDate date not null,
MemberID bigint,
constraint fk_Member_SharedFile foreign key (MemberID) references Member(MemberID)on update cascade on delete cascade
)
